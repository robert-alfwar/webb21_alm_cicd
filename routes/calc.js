const express = require('express');
const Calculator = require('../service/calc');

const router = express.Router();

/* GET calc listing. */
router.get('/', (req, res, next) => {
  res.send('This is the calculator endpoint');
});

router.get('/add', (req, res, next) => {
  const calculator = new Calculator();
  const [term1 = 0, term2 = 0] = req.query.val || [];
  calculator.add(Number(term1));
  calculator.add(Number(term2));
  res.send(`The sum is ${calculator.getResult()}`);
});

module.exports = router;
