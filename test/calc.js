const assert = require('assert');
const Calculator = require('../service/calc');

describe('Calculator', function () {
  beforeEach(function () {
    this.calc = new Calculator();
  });

  it('should be able to instantiate a Calculator', function () {
    const calculator = new Calculator();
    assert(typeof (calculator) === 'object');
  });
  describe('#add', function () {
    it('should return 2 with term 1 and current value 1', function () {
      this.calc.add(1);
      this.calc.add(1);
      assert.strictEqual(this.calc.getResult(), 2);
    });
  });
});
